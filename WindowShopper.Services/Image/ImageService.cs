﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Helpers;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Product;
using WindowShopper.DTO.User;
using WindowShopper.Services.Base;

namespace WindowShopper.Services.Image
{
    public class ImageService : BaseService
    {
        public string FileStorage { get; set; }
        public ImageService()
        {
            this.FileStorage = Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory) + "\\Images";
        }
        public Result<String> SaveProfilePicture(string base64Image, UserDTO _user)
        {
            Result<string> _result = new Result<string>();
            string pictureFolderLocation = FileStorage + "\\ProfilePictures\\" + _user.ID.ToString();
            string ImageName = "\\UserProfilePicture_" + _user.ID.ToString() + ".png";
            string pictureFilePath = pictureFolderLocation + "\\" + ImageName;
            byte[] imageAsByteArray = Convert.FromBase64String(Regex.Replace(base64Image, "data:image/(png|jpg|gif|jpeg|pjpeg|x-png);base64,", ""));
            WebImage img = new WebImage(imageAsByteArray);
            img.Resize(200, 200);
            imageAsByteArray = img.GetBytes();
            try
            {
                if (!Directory.Exists(pictureFolderLocation))
                {
                    Directory.CreateDirectory(pictureFolderLocation);
                }
                if (System.IO.File.Exists(pictureFilePath))
                {
                    System.IO.File.Delete(pictureFilePath);
                }
                System.IO.File.WriteAllBytes(pictureFilePath, imageAsByteArray);
                _result.TransactionResult = pictureFolderLocation;
                _result.IsSucceeded = true;
                _result.ResultMessage.Add("Save Image Succeeded!");
            }
            catch (Exception ex)
            {
                _result.TransactionResult = "";
                _result.IsSucceeded = false;
                _result.ResultMessage.Add("Save Image Failed!");
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<String> SaveProductPictures(string base64ImageDefault, string base64ImageBig, string base64ImageSmall, ProductDTO _product)
        {
            Result<string> _result = new Result<string>();
            string pictureFolderLocation = FileStorage + "\\ProductPictures\\" + _product.ID.ToString();

            #region DefaultImage
            string ImageName = "ProductPicture_" + _product.ID.ToString() + ".png";
            string pictureFilePath = pictureFolderLocation + "\\" + ImageName;
            byte[] imageAsByteArray = Convert.FromBase64String(Regex.Replace(base64ImageDefault, "data:image/(png|jpg|gif|jpeg|pjpeg|x-png);base64,", ""));
            WebImage img = new WebImage(imageAsByteArray);
            img.Resize(255, 237);
            imageAsByteArray = img.GetBytes();
            #endregion

            #region BigImage
            string BigImageName = "ProductPicture_" + _product.ID.ToString() + "_2.png";
            string bigPictureFilePath = pictureFolderLocation + "\\" + BigImageName;
            byte[] bigImageAsByteArray = Convert.FromBase64String(Regex.Replace(base64ImageBig, "data:image/(png|jpg|gif|jpeg|pjpeg|x-png);base64,", ""));
            WebImage bigImg = new WebImage(bigImageAsByteArray);
            bigImg.Resize(329, 380);
            bigImageAsByteArray = bigImg.GetBytes();
            #endregion

            #region SmallImage
            string SmallImageName = "ProductPicture_" + _product.ID.ToString() + "_3.png";
            string smallPictureFilePath = pictureFolderLocation + "\\" + SmallImageName;
            byte[] smallImageAsByteArray = Convert.FromBase64String(Regex.Replace(base64ImageSmall, "data:image/(png|jpg|gif|jpeg|pjpeg|x-png);base64,", ""));
            WebImage smallImg = new WebImage(smallImageAsByteArray);
            smallImg.Resize(84, 85);
            smallImageAsByteArray = smallImg.GetBytes();
            #endregion

            try
            {
                if (!Directory.Exists(pictureFolderLocation))
                {
                    Directory.CreateDirectory(pictureFolderLocation);
                }
                if (System.IO.File.Exists(pictureFilePath))
                {
                    System.IO.File.Delete(pictureFilePath);
                }
                System.IO.File.WriteAllBytes(pictureFilePath, imageAsByteArray);
                System.IO.File.WriteAllBytes(bigPictureFilePath, bigImageAsByteArray);
                System.IO.File.WriteAllBytes(smallPictureFilePath, smallImageAsByteArray);
                _result.TransactionResultList = new List<string>()
                {
                    pictureFilePath.Replace(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory),""),
                    bigPictureFilePath.Replace(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory),""),
                    smallPictureFilePath.Replace(Path.GetDirectoryName(System.AppDomain.CurrentDomain.BaseDirectory),"")
                };
                _result.IsSucceeded = true;
                _result.ResultMessage.Add("Save Images Succeeded!");
            }
            catch (Exception ex)
            {
                _result.TransactionResult = "";
                _result.IsSucceeded = false;
                _result.ResultMessage.Add("Save Images Failed!");
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        private byte[] GetImageData(string base64String)
        {
            byte[] buffer = Convert.FromBase64String(base64String);
            byte[] imageBytes = new byte[buffer.Length];

            try
            {
                imageBytes = new System.Security.Cryptography.FromBase64Transform().TransformFinalBlock(buffer, 0, buffer.Length);
            }
            catch (Exception)
            {
                imageBytes = buffer;
            }

            return imageBytes;
        }
        private System.Drawing.Image GetImageFromByteArray(byte[] byteArray)
        {
            using (var ms = new MemoryStream(byteArray))
            {
                return System.Drawing.Image.FromStream(ms);
            }
        }
    }
}
