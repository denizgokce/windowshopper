# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is Full Scale E-Commerce project which covers a lot of features.
* 1.0
* [Learn Markdown](https://bitbucket.org/DenizGokce/windowshopper)
* Project URL [Window Shopper](http://window-shopper.azurewebsites.net/)

### Architecture and Frameworks That Project Includes ###

* N-Tier Architecture
* MVC 5
* Repository Pattern
* Dependency Injection with Unity
* EntityFramework for Azure SQL Server
* Microsoft UnitTest Framework

### Who do I talk to? ###

* Deniz Gokce
* denizgokce93@gmail.com