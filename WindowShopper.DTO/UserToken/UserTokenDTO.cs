﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;

namespace WindowShopper.DTO.UserToken
{
    public class UserTokenDTO : BaseDTO
    {
        public int userID { get; set; }
        public string email { get; set; }
        public string token { get; set; }
        public System.Guid tokenKey { get; set; }
    }
}
