﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;

namespace WindowShopper.DTO.UserTypes
{
    public class UserTypeDTO : BaseDTO
    {
        public string type { get; set; }
        public UserType _type { get; set; }
    }
}
