﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;

namespace WindowShopper.DTO.ProductTypes
{
    public class ProductTypeDTO : BaseDTO
    {
        public string type { get; set; }
        public ProductType _type { get; set; }
    }
}
