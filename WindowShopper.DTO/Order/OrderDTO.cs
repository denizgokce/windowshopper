﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;
using WindowShopper.DTO.OrderProduct;
using WindowShopper.DTO.Product;

namespace WindowShopper.DTO.Order
{
    public class OrderDTO : BaseDTO
    {
        public int buyerID { get; set; }
        public double totalPrice { get; set; }
        /************************************************/
        public List<OrderedProductDTO> orderedProducts { get; set; }
    }
}
