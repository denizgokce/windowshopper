﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;

namespace WindowShopper.DTO.UserInformation
{
    public class UserInformationDTO : BaseDTO
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public Nullable<System.DateTime> dateOfBirth { get; set; }
        public string SSN { get; set; }
        public string profilePicture { get; set; }
        public string phoneNumber { get; set; }
    }
}
