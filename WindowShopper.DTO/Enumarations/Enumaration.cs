﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowShopper
{
    public enum UserType
    {
        Seller = 1,
        Buyer = 2
    }
    public enum ProductType
    {
        Electronic = 1,
        Clothing = 2
    }
    public enum Status
    {
        Active = 1,
        Inactive = -1
    }
}
