﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;
using WindowShopper.DTO.Product;

namespace WindowShopper.DTO.OrderProduct
{
    public class OrderedProductDTO : BaseDTO
    {
        public int orderID { get; set; }
        public int productID { get; set; }
        public int quantity { get; set; }
        public double productTotalPrice { get; set; }
        /************************************************/
        public ProductDTO product { get; set; }
    }
}
