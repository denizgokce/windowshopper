﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WindowShopper.DTO.Generic
{


    [DataContract]
    public class Result
    {
        [DataMember]
        public bool IsSucceeded { get; set; }

        private IList<string> _resultMessage = new List<string>();
        [DataMember]
        public IList<string> ResultMessage
        {
            get { return _resultMessage; }
            set { _resultMessage = value; }
        }


    }
    [DataContract]
    public class Result<T> : Result
    {

        [DataMember]
        public T TransactionResult { get; set; }

        private List<T> transactionResultList = new List<T>();
        [DataMember]
        public List<T> TransactionResultList
        {
            get
            {
                return transactionResultList;
            }
            set
            {
                transactionResultList = value;
            }
        }

    }
}
