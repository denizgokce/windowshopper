﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;
using WindowShopper.DTO.User;

namespace WindowShopper.DTO.Product
{
    public class ProductDTO : BaseDTO
    {
        public int productType { get; set; }
        public ProductType _productType { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public double price { get; set; }
        public int ownerID { get; set; }
        public int quantity { get; set; }
        public string pictures { get; set; }
        public List<string> _pictureList { get; set; }
        public int _pageNumber { get; set; }
        public int _pageSize { get; set; }
        /*********************************************/
        public UserDTO user { get; set; }
    }
}
