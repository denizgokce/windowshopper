﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;
using WindowShopper.DTO.Message;
using WindowShopper.DTO.Product;
using WindowShopper.DTO.UserInformation;

namespace WindowShopper.DTO.User
{
    public class UserDTO : BaseDTO
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int userType { get; set; }
        public UserType _userType { get; set; }
        /********************************************/
        public List<ProductDTO> products { get; set; }
        public UserInformationDTO userInformation { get; set; }
    }
}
