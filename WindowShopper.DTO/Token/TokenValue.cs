﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowShopper.DTO.Token
{
    public class TokenValue
    {
        public string Token { get; set; }
        public System.Guid UniqueKey { get; set; }
    }
}
