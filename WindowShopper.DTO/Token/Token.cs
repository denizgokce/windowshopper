﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.User;

namespace WindowShopper.DTO.Token
{
    public class Token
    {
        public Guid UniqueKey { get; set; }
        public UserDTO User { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public Boolean IsMobile { get; set; }
    }
}
