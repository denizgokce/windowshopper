﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowShopper.DTO.Base
{
    public class BaseDTO
    {
        public int ID { get; set; }
        public int status { get; set; }
        public Status _status { get; set; }
        public System.DateTime createdDate { get; set; }
        public Nullable<System.DateTime> updatedDate { get; set; }
        public System.Guid uniqueID { get; set; }
    }
}
