﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DTO.Base;
using WindowShopper.DTO.User;

namespace WindowShopper.DTO.Message
{
    public class MessageDTO : BaseDTO
    {
        public int fromID { get; set; }
        public int toID { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public bool isRed { get; set; }
        public int _pageNumber { get; set; }
        public int _pageSize { get; set; }
        /********************************************/
        public UserDTO FromUser { get; set; }
        public UserDTO ToUser { get; set; }
    }
}
