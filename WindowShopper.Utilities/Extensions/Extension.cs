﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.Utilities.Security;

namespace WindowShopper
{
    public static class Extension
    {
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        public static int ToInt(this object obj)
        {
            return Convert.ToInt32(obj);
        }
        public static bool ToBoolean(this object obj)
        {
            return Convert.ToBoolean(obj);
        }
        public static double ToDouble(this object obj)
        {
            return Convert.ToDouble(obj);
        }
        public static string ToJson(this Object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static T ToType<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
        public static string PasswordEncrypt(this string password)
        {
            return Cryptography.PasswordEncrypt(password);
        }
        public static string PasswordDecrypt(this string password)
        {
            return Cryptography.PasswordDecrypt(password);
        }
        public static string GetTimeDiffInString(this DateTime CreatedDate)
        {
            TimeSpan span = (CreatedDate - DateTime.Now);
            if (span.Days > 0)
            {
                return span.Days.ToString() + " Days Ago";
            }
            if (span.Hours>0)
            {
                return span.Hours.ToString() + " Hours Ago";
            }
            if (span.Minutes > 0)
            {
                return span.Minutes.ToString() + " Minutes Ago";
            }
            if (span.Seconds > 0)
            {
                return span.Seconds.ToString() + " Seconds Ago";
            }
            return CreatedDate.ToString();
        }
    }
}
