﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using WindowShopper.Utilities.Security;
using PagedList;
using WindowShopper.DTO.Base;

namespace WindowShopper.DAO
{
    public static class Extension
    {
        public static DTO.User.UserDTO ToDTO(this DAL.DatabaseModel.User obj)
        {
            return new DTO.User.UserDTO()
            {
                ID = obj.ID,
                username = obj.Username,
                password = obj.Password.PasswordDecrypt(),
                email = obj.Email,
                userType = obj.UserType,
                _userType = obj.UserType.GetUserType(),
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus(),
                userInformation = (obj.UserInformation != null) ? obj.UserInformation.ToDTO() : null
            };
        }
        public static List<DTO.User.UserDTO> ToDTO(this IList<DAL.DatabaseModel.User> objList)
        {
            return (from r in objList
                    select new DTO.User.UserDTO()
                    {
                        ID = r.ID,
                        username = r.Username,
                        password = Cryptography.PasswordDecrypt(r.Password),
                        email = r.Email,
                        userType = r.UserType,
                        _userType = r.UserType.GetUserType(),
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus()
                    }).ToList();
        }
        public static DTO.Product.ProductDTO ToDTO(this DAL.DatabaseModel.Product obj)
        {
            return new DTO.Product.ProductDTO()
            {
                ID = obj.ID,
                name = obj.Name,
                description = obj.Description,
                price = obj.Price,
                productType = obj.ProductType,
                _productType = obj.ProductType.GetProductType(),
                pictures = obj.Pictures,
                _pictureList = obj.Pictures.GetProductPictures(),
                quantity = obj.Quantity,
                ownerID = obj.OwnerID,
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus(),
                user = (obj.User != null) ? obj.User.ToDTO() : null
            };
        }
        public static List<DTO.Product.ProductDTO> ToDTO(this IList<DAL.DatabaseModel.Product> objList)
        {
            return (from r in objList
                    select new DTO.Product.ProductDTO()
                    {
                        ID = r.ID,
                        name = r.Name,
                        description = r.Description,
                        price = r.Price,
                        productType = r.ProductType,
                        _productType = r.ProductType.GetProductType(),
                        pictures = r.Pictures,
                        _pictureList = r.Pictures.GetProductPictures(),
                        quantity = r.Quantity,
                        ownerID = r.OwnerID,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus(),
                        user = (r.User != null) ? r.User.ToDTO() : null
                    }).ToList();
        }
        public static List<DTO.Product.ProductDTO> ToDTO(this IPagedList<DAL.DatabaseModel.Product> objList)
        {
            return (from r in objList
                    select new DTO.Product.ProductDTO()
                    {
                        ID = r.ID,
                        name = r.Name,
                        description = r.Description,
                        price = r.Price,
                        productType = r.ProductType,
                        _productType = r.ProductType.GetProductType(),
                        pictures = r.Pictures,
                        _pictureList = r.Pictures.GetProductPictures(),
                        quantity = r.Quantity,
                        ownerID = r.OwnerID,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus(),
                        user = (r.User != null) ? r.User.ToDTO() : null
                    }).ToList();
        }
        public static DTO.UserTypes.UserTypeDTO ToDTO(this DAL.DatabaseModel.UserType obj)
        {
            return new DTO.UserTypes.UserTypeDTO()
            {
                ID = obj.ID,
                type = obj.Type,
                _type = obj.Type.ToEnum<UserType>(),
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus()
            };
        }
        public static List<DTO.UserTypes.UserTypeDTO> ToDTO(this IList<DAL.DatabaseModel.UserType> objList)
        {
            return (from r in objList
                    select new DTO.UserTypes.UserTypeDTO()
                    {
                        ID = r.ID,
                        type = r.Type,
                        _type = r.Type.ToEnum<UserType>(),
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus()
                    }).ToList();
        }
        public static DTO.ProductTypes.ProductTypeDTO ToDTO(this DAL.DatabaseModel.ProductType obj)
        {
            return new DTO.ProductTypes.ProductTypeDTO()
            {
                ID = obj.ID,
                type = obj.Type,
                _type = obj.Type.ToEnum<ProductType>(),
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus()
            };
        }
        public static List<DTO.ProductTypes.ProductTypeDTO> ToDTO(this IList<DAL.DatabaseModel.ProductType> objList)
        {
            return (from r in objList
                    select new DTO.ProductTypes.ProductTypeDTO()
                    {
                        ID = r.ID,
                        type = r.Type,
                        _type = r.Type.ToEnum<ProductType>(),
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus()
                    }).ToList();
        }
        public static DTO.UserToken.UserTokenDTO ToDTO(this DAL.DatabaseModel.UserToken obj)
        {
            return new DTO.UserToken.UserTokenDTO()
            {
                ID = obj.ID,
                token = obj.Token,
                tokenKey = obj.TokenKey,
                userID = obj.UserID,
                email = obj.Email,
                status = obj.Status,
                _status = obj.Status.GetStatus(),
                createdDate = obj.CreatedDate,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID
            };
        }
        public static DTO.UserInformation.UserInformationDTO ToDTO(this DAL.DatabaseModel.UserInformation obj)
        {
            return new DTO.UserInformation.UserInformationDTO()
            {
                ID = obj.ID,
                firstName = obj.FirstName,
                lastName = obj.LastName,
                dateOfBirth = obj.DateOfBirth,
                profilePicture = obj.ProfilePicture.GetProfilePicture(),
                SSN = obj.SSN,
                phoneNumber = obj.PhoneNumber,
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus()
            };
        }
        public static DTO.Message.MessageDTO ToDTO(this DAL.DatabaseModel.Message obj)
        {
            return new DTO.Message.MessageDTO()
            {
                ID = obj.ID,
                content = obj.Content,
                title = obj.Title,
                fromID = obj.FromID,
                toID = obj.ToID,
                isRed = obj.IsRed,
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus(),
                FromUser = (obj.User != null) ? obj.User.ToDTO() : null,
                ToUser = (obj.User1 != null) ? obj.User1.ToDTO() : null
            };
        }
        public static List<DTO.Message.MessageDTO> ToDTO(this IList<DAL.DatabaseModel.Message> objList)
        {
            return (from r in objList
                    select new DTO.Message.MessageDTO()
                    {
                        ID = r.ID,
                        content = r.Content,
                        title = r.Title,
                        fromID = r.FromID,
                        toID = r.ToID,
                        isRed = r.IsRed,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus(),
                        FromUser = (r.User != null) ? r.User.ToDTO() : null,
                        ToUser = (r.User1 != null) ? r.User1.ToDTO() : null
                    }).ToList();
        }
        public static List<DTO.Message.MessageDTO> ToDTO(this ICollection<DAL.DatabaseModel.Message> objList)
        {
            return (from r in objList
                    select new DTO.Message.MessageDTO()
                    {
                        ID = r.ID,
                        content = r.Content,
                        title = r.Title,
                        fromID = r.FromID,
                        toID = r.ToID,
                        isRed = r.IsRed,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus(),
                        FromUser = (r.User != null) ? r.User.ToDTO() : null,
                        ToUser = (r.User1 != null) ? r.User1.ToDTO() : null
                    }).ToList();
        }
        public static List<DTO.Message.MessageDTO> ToDTO(this IPagedList<DAL.DatabaseModel.Message> objList)
        {
            return (from r in objList
                    select new DTO.Message.MessageDTO()
                    {
                        ID = r.ID,
                        content = r.Content,
                        title = r.Title,
                        fromID = r.FromID,
                        toID = r.ToID,
                        isRed = r.IsRed,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus(),
                        FromUser = (r.User != null) ? r.User.ToDTO() : null,
                        ToUser = (r.User1 != null) ? r.User1.ToDTO() : null
                    }).ToList();
        }
        public static DTO.Order.OrderDTO ToDTO(this DAL.DatabaseModel.Order obj)
        {
            return new DTO.Order.OrderDTO()
            {
                ID = obj.ID,
                buyerID = obj.BuyerID,
                totalPrice = obj.TotalPrice,
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus(),
                orderedProducts = (obj.OrderedProduct != null) ? obj.OrderedProduct.ToDTO() : null
            };
        }
        public static List<DTO.Order.OrderDTO> ToDTO(this IList<DAL.DatabaseModel.Order> objList)
        {
            return (from r in objList
                    select new DTO.Order.OrderDTO()
                    {
                        ID = r.ID,
                        buyerID = r.BuyerID,
                        totalPrice = r.TotalPrice,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus(),
                        orderedProducts = (r.OrderedProduct != null) ? r.OrderedProduct.ToDTO() : null
                    }).ToList();
        }
        public static DTO.OrderProduct.OrderedProductDTO ToDTO(this DAL.DatabaseModel.OrderedProduct obj)
        {
            return new DTO.OrderProduct.OrderedProductDTO()
            {
                ID = obj.ID,
                orderID = obj.OrderID,
                productID = obj.ProductID,
                productTotalPrice = obj.ProductTotalPrice,
                quantity = obj.Quantity,
                createdDate = obj.CreatedDate,
                status = obj.Status,
                updatedDate = obj.UpdatedDate,
                uniqueID = obj.UniqueID,
                _status = obj.Status.GetStatus()
            };
        }
        public static List<DTO.OrderProduct.OrderedProductDTO> ToDTO(this IList<DAL.DatabaseModel.OrderedProduct> objList)
        {
            return (from r in objList
                    select new DTO.OrderProduct.OrderedProductDTO()
                    {
                        ID = r.ID,
                        orderID = r.OrderID,
                        productID = r.ProductID,
                        productTotalPrice = r.ProductTotalPrice,
                        quantity = r.Quantity,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus()
                    }).ToList();
        }
        public static List<DTO.OrderProduct.OrderedProductDTO> ToDTO(this ICollection<DAL.DatabaseModel.OrderedProduct> objList)
        {
            return (from r in objList
                    select new DTO.OrderProduct.OrderedProductDTO()
                    {
                        ID = r.ID,
                        orderID = r.OrderID,
                        productID = r.ProductID,
                        productTotalPrice = r.ProductTotalPrice,
                        quantity = r.Quantity,
                        createdDate = r.CreatedDate,
                        status = r.Status,
                        updatedDate = r.UpdatedDate,
                        uniqueID = r.UniqueID,
                        _status = r.Status.GetStatus()
                    }).ToList();
        }

    }
    public static class EnumConversion
    {
        public static UserType GetUserType(this int type)
        {
            return ((UserType)type);
        }
        public static Status GetStatus(this int status)
        {
            return ((Status)status);
        }
        public static ProductType GetProductType(this int type)
        {
            return ((ProductType)type);
        }
    }
    public static class Conversion
    {
        public static string GetProfilePicture(this string picture)
        {
            if (!string.IsNullOrEmpty(picture))
            {
                return picture;
            }
            else
            {
                return @"\Images\home\ProfileDefault.png";
            }
        }
        public static List<String> GetProductPictures(this string pictures)
        {
            if (!string.IsNullOrEmpty(pictures))
            {
                return pictures.ToType<List<String>>();
            }
            else
            {
                return new List<string>()
                {
                    @"\Images\home\ProductDefault.png",
                    @"\Images\home\ProductDefault_2.png",
                    @"\Images\home\ProductDefault_3.png"
                };
            }
        }
    }
}
