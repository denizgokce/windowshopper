﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using WindowShopper.DAO.Mappings;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;

namespace WindowShopper.DAO
{
    public class Repository<T> : IDisposable, IRepository<T> where T : class
    {
        public IDataContextFactory _dataContextFactory;
        public Repository(IDataContextFactory dataContextFactory)
        {
            _dataContextFactory = dataContextFactory;
        }//End Repository Constructor

        #region CRUD Operations with Generic Result
        public Result<T> AddItem(T entity)
        {
            Result<T> _result = new Result<T>();
            try
            {
                GetTable.Add(entity);
                this._dataContextFactory.SaveAll();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Insert_Success);
                _result.TransactionResult = entity;
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> UpdateItem(T entity)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _dataContextFactory.Context.Entry<T>(entity).State = System.Data.Entity.EntityState.Modified;
                _dataContextFactory.SaveAll();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Update_Success);
                _result.TransactionResult = entity;
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Update_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> DeleteItem(T entity)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _dataContextFactory.Context.Entry<T>(entity).State = System.Data.Entity.EntityState.Deleted;
                _dataContextFactory.SaveAll();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Delete_Success);
                _result.TransactionResult = entity;
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Delete_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }//End Method
        #endregion

        #region Get List

        public Result<T> GetAll()
        {
            Result<T> _result = new Result<T>();
            try
            {
                //Type typeParameterType = typeof(T);
                //Convert.ChangeType(obj, t);
                _result.TransactionResultList = GetTable.ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> GetAll(int currPage, int pageSize)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.Skip((currPage - 1) * pageSize).Take(pageSize).ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> GetAllWithOrderByAsc(Func<T, object> orderByAscField, int currPage, int pageSize)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.OrderBy(orderByAscField).Skip((currPage - 1) * pageSize).Take(pageSize).ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> GetAllWithOrderByDesc(Func<T, object> orderByDescField, int currPage, int pageSize)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.OrderByDescending(orderByDescField).Skip((currPage - 1) * pageSize).Take(pageSize).ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        #endregion

        #region Get List By Query

        public Result<T> GetItemListByQuery(Func<T, bool> exp)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.Where(exp).ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> GetItemListByQuery(Func<T, bool> exp, int currPage, int pageSize)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.Where(exp).Skip((currPage - 1) * pageSize).Take(pageSize).ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> GetItemListByQueryWithOrderByAsc(Func<T, bool> exp, Func<T, object> orderByAscField, int currPage, int pageSize)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.Where(exp).OrderBy(orderByAscField).Skip((currPage - 1) * pageSize).Take(pageSize).ToList();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        public Result<T> GetItemListByQueryWithOrderByDesc(Func<T, bool> exp, Func<T, object> orderByDescField, int currPage, int pageSize)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResultList = GetTable.Where(exp).OrderByDescending(orderByDescField).Skip((currPage - 1) * pageSize).Take(pageSize).ToList();
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        #endregion

        public Result<T> GetSingle(Func<T, bool> exp)
        {
            Result<T> _result = new Result<T>();
            try
            {
                _result.TransactionResult = GetTable.FirstOrDefault(exp);
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

        #region Properties

        //private string PrimaryKeyName
        //{
        //    get { return TableMetadata.RowType.IdentityMembers[0].Name; }
        //}

        private System.Data.Entity.DbSet<T> GetTable
        {
            get { return _dataContextFactory.Context.Set<T>(); }
        }

        //private System.Data.Linq.Mapping.MetaTable TableMetadata
        //{
        //    get { return _dataContextFactory.Context.Mapping.GetTable(typeof(T)); }
        //}

        //private System.Data.Linq.Mapping.MetaType ClassMetadata
        //{
        //    get { return _dataContextFactory.Context.Mapping.GetMetaType(typeof(T)); }
        //}

        #endregion


        public int GetItemCountByQuery(Func<T, bool> exp)
        {
            return GetTable.Count(exp);
        }
        public int GetItemCount()
        {
            return GetTable.Count();
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }//End Class

}//End Namespace
