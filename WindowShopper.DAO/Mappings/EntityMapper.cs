﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DTO.Message;
using WindowShopper.DTO.Order;
using WindowShopper.DTO.OrderProduct;
using WindowShopper.DTO.Product;
using WindowShopper.DTO.ProductTypes;
using WindowShopper.DTO.User;
using WindowShopper.DTO.UserInformation;
using WindowShopper.DTO.UserToken;
using WindowShopper.DTO.UserTypes;

namespace WindowShopper.DAO.Mappings
{
    public class EntityMapper
    {
        private static Dictionary<Type, Type> _entityMapper;
        private static volatile EntityMapper instance;
        private static object syncRoot = new Object();

        private EntityMapper() { }

        public static EntityMapper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                        {
                            instance = new EntityMapper();
                            _entityMapper = new Dictionary<Type, Type>()
                            {
                                { typeof(Message), typeof(MessageDTO) },
                                { typeof(Order), typeof(OrderDTO) },
                                { typeof(OrderedProduct), typeof(OrderedProductDTO) },
                                { typeof(Product), typeof(ProductDTO) },
                                { typeof(DAL.DatabaseModel.ProductType), typeof(ProductTypeDTO) },
                                { typeof(User), typeof(UserDTO) },
                                { typeof(UserInformation), typeof(UserInformationDTO) },
                                { typeof(UserToken), typeof(UserTokenDTO) },
                                { typeof(DAL.DatabaseModel.UserType), typeof(UserTypeDTO) }
                            };

                        }
                    }
                }

                return instance;
            }
        }
        public Type Resolve<T>()
        {
            return _entityMapper[typeof(T)];
        }

    }
}
