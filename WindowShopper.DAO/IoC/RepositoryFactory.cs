﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowShopper.DAO
{
    public class RepositoryFactory<T>
    {
        public static T CreateInstance()
        {
            IoC ioc = new IoC();
            return ioc.Resolve<T>();
        }
    }
}
