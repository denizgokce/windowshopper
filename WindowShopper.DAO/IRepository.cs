﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using WindowShopper.DTO.Generic;

namespace WindowShopper.DAO
{
    public interface IRepository<T> where T : class
    {
        Result<T> AddItem(T item);
        Result<T> UpdateItem(T item);
        Result<T> DeleteItem(T item);
        Result<T> GetAll();
        Result<T> GetAll(int currPage, int pageSize);
        Result<T> GetAllWithOrderByDesc(Func<T, object> orderByDescField, int currPage, int pageSize);
        Result<T> GetAllWithOrderByAsc(Func<T, object> orderByAscField, int currPage, int pageSize);
        Result<T> GetItemListByQuery(Func<T, bool> exp, int currPage, int pageSize);
        Result<T> GetItemListByQueryWithOrderByDesc(Func<T, bool> exp, Func<T, object> orderByDescField, int currPage, int pageSize);
        Result<T> GetItemListByQueryWithOrderByAsc(Func<T, bool> exp, Func<T, object> orderByAscField, int currPage, int pageSize);
        Result<T> GetItemListByQuery(Func<T, bool> exp);
        Result<T> GetSingle(Func<T, bool> exp);
        int GetItemCountByQuery(Func<T, bool> exp);
        int GetItemCount();

    }
}
