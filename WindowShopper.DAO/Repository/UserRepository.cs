﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.User;

namespace WindowShopper.DAO.Repository
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<UserDTO> UserLogin(UserDTO obj)
        {
            Result<UserDTO> _result = new Result<UserDTO>();
            try
            {
                var detail = (from r in _dataContextFactory.Context.User
                              where (r.Email == obj.email || r.Username == obj.username) &&
                                    r.Password == obj.password
                              select r).FirstOrDefault();

                if (detail == null)
                {
                    _result.TransactionResult = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResult = detail.ToDTO() ?? null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserDTO> GetUser(UserDTO obj)
        {
            Result<UserDTO> _result = new Result<UserDTO>();
            try
            {
                var detail = (from r in _dataContextFactory.Context.User
                              where r.ID == obj.ID
                              select r).FirstOrDefault();

                if (detail == null)
                {
                    _result.TransactionResult = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResult = detail.ToDTO() ?? null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserDTO> GetUserByUsername(UserDTO obj)
        {
            Result<UserDTO> _result = new Result<UserDTO>();
            try
            {
                var detail = (from r in _dataContextFactory.Context.User
                              where r.Username == obj.username
                              select r).FirstOrDefault();

                if (detail == null)
                {
                    _result.TransactionResult = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResult = detail.ToDTO() ?? null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserDTO> AddUser(UserDTO obj)
        {
            Result<UserDTO> _result = new Result<UserDTO>();
            try
            {
                var existRecord = (from r in _dataContextFactory.Context.User
                                   where r.Email == obj.email ||
                                         r.Username == obj.username
                                   select r).FirstOrDefault();

                if (existRecord == null)
                {
                    User newRecord = new User()
                    {
                        Username = obj.username,
                        Password = obj.password.PasswordEncrypt(),
                        Email = obj.email,
                        UserType = obj._userType.ToInt(),
                        CreatedDate = System.DateTime.Now,
                        Status = Status.Active.ToInt(),
                        UniqueID = System.Guid.NewGuid()
                    };
                    _dataContextFactory.Context.User.Add(newRecord);
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = newRecord.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Insert_Success);
                }
                else
                {
                    _result.TransactionResult = existRecord.ToDTO() ?? null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserDTO> UpdateUser(UserDTO obj)
        {
            Result<UserDTO> _result = new Result<UserDTO>();
            try
            {
                var existRecord = (from r in _dataContextFactory.Context.User
                                   where r.ID == obj.ID
                                   select r).FirstOrDefault();

                if (existRecord != null)
                {
                    existRecord.Username = obj.username;
                    existRecord.Email = obj.email;
                    existRecord.Password = obj.password;
                    existRecord.UserType = obj.userType;
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = existRecord.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Insert_Success);
                }
                else
                {
                    _result.TransactionResult = existRecord.ToDTO() ?? null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

    }
}
