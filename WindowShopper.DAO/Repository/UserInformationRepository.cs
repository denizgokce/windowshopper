﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.UserInformation;

namespace WindowShopper.DAO.Repository
{
    public class UserInformationRepository : Repository<UserInformation>
    {
        public UserInformationRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<UserInformationDTO> UpdateUserInformation(UserInformationDTO obj)
        {
            Result<UserInformationDTO> _result = new Result<UserInformationDTO>();
            try
            {
                var existRecord = (from r in _dataContextFactory.Context.UserInformation
                                   where r.ID == obj.ID
                                   select r).FirstOrDefault();

                if (existRecord == null)
                {
                    UserInformation newRecord = new UserInformation()
                    {
                        ID = obj.ID,
                        FirstName = obj.firstName,
                        LastName = obj.lastName,
                        ProfilePicture = obj.profilePicture,
                        SSN = obj.SSN,
                        DateOfBirth = obj.dateOfBirth,
                        CreatedDate = System.DateTime.Now,
                        Status = Status.Active.ToInt(),
                        UniqueID = System.Guid.NewGuid()
                    };
                    _dataContextFactory.Context.UserInformation.Add(newRecord);
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = newRecord.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Update_Success);
                }
                else
                {
                    existRecord.FirstName = obj.firstName ?? existRecord.FirstName;
                    existRecord.LastName = obj.lastName ?? existRecord.LastName;
                    existRecord.ProfilePicture = obj.profilePicture ?? existRecord.ProfilePicture;
                    existRecord.SSN = obj.SSN ?? existRecord.SSN;
                    existRecord.DateOfBirth = obj.dateOfBirth ?? existRecord.DateOfBirth;
                    existRecord.UpdatedDate = System.DateTime.Now;
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = existRecord.ToDTO() ?? null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Update_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Update_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserInformationDTO> GetUserInformation(UserInformationDTO obj)
        {
            Result<UserInformationDTO> _result = new Result<UserInformationDTO>();
            try
            {
                var detail = (from r in _dataContextFactory.Context.UserInformation
                              where r.ID == obj.ID
                              select r).FirstOrDefault();

                if (detail == null)
                {
                    _result.TransactionResult = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResult = detail.ToDTO() ?? null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
