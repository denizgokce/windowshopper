﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Message;

namespace WindowShopper.DAO.Repository
{
    public class MessageRepository : Repository<Message>
    {
        public MessageRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<MessageDTO> GetLatestRecievedMessagesByPage(MessageDTO obj)
        {
            Result<MessageDTO> _result = new Result<MessageDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.Message
                               where r.ToID == obj.toID
                               select r).OrderByDescending(x => x.CreatedDate).ToPagedList(obj._pageNumber, obj._pageSize);
                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<MessageDTO> GetLatestSentMessagesByPage(MessageDTO obj)
        {
            Result<MessageDTO> _result = new Result<MessageDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.Message
                               where r.FromID == obj.fromID
                               select r).OrderByDescending(x => x.CreatedDate).ToPagedList(obj._pageNumber, obj._pageSize);
                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<int> GetUnreadMessageCount(MessageDTO obj)
        {
            Result<int> _result = new Result<int>();
            try
            {
                int detailCount = (from r in _dataContextFactory.Context.Message
                                   where r.ToID == obj.toID && r.IsRed == false
                                   select r).Count();
                _result.TransactionResult = detailCount;
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Select_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<MessageDTO> UpdateUnreadMessages(MessageDTO obj)
        {
            Result<MessageDTO> _result = new Result<MessageDTO>();
            try
            {
                var existRecords = (from r in _dataContextFactory.Context.Message
                                    where r.ToID == obj.toID && r.IsRed == false
                                    select r).ToList();
                if (existRecords != null)
                {
                    existRecords.ToList().ForEach(x =>
                    {
                        x.IsRed = true;
                    });
                    _dataContextFactory.SaveAll();
                    _result.TransactionResultList = existRecords.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<MessageDTO> AddMessage(MessageDTO obj)
        {
            Result<MessageDTO> _result = new Result<MessageDTO>();
            try
            {
                Message newRecord = new Message()
                {
                    FromID = obj.fromID,
                    ToID = obj.toID,
                    Title = obj.title,
                    Content = obj.content,
                    CreatedDate = System.DateTime.Now,
                    Status = Status.Active.ToInt(),
                    UniqueID = System.Guid.NewGuid()
                };
                _dataContextFactory.Context.Message.Add(newRecord);
                _dataContextFactory.SaveAll();
                _result.IsSucceeded = true;
                _result.TransactionResult = newRecord.ToDTO();
                _result.ResultMessage.Add(DBContextActions.Insert_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }

    }
}
