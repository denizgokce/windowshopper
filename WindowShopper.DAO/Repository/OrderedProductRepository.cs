﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.OrderProduct;

namespace WindowShopper.DAO.Repository
{
    public class OrderedProductRepository : Repository<OrderedProduct>
    {
        public OrderedProductRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<OrderedProductDTO> AddOrderedProducts(List<OrderedProductDTO> objList)
        {
            Result<OrderedProductDTO> _result = new Result<OrderedProductDTO>();
            try
            {
                List<OrderedProduct> newRecords = new List<OrderedProduct>();
                foreach (var orderedProcuct in objList)
                {
                    newRecords.Add(new OrderedProduct()
                    {
                        OrderID = orderedProcuct.orderID,
                        ProductID = orderedProcuct.productID,
                        Quantity = orderedProcuct.quantity,
                        ProductTotalPrice = orderedProcuct.productTotalPrice,
                        CreatedDate = System.DateTime.Now,
                        Status = Status.Active.ToInt(),
                        UniqueID = System.Guid.NewGuid()
                    });
                }
                _dataContextFactory.Context.OrderedProduct.AddRange(newRecords);
                _dataContextFactory.SaveAll();
                _result.TransactionResultList = newRecords.ToDTO();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Insert_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
