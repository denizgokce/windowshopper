﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Order;

namespace WindowShopper.DAO.Repository
{
    public class OrderRepository : Repository<Order>
    {
        public OrderRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<OrderDTO> AddOrder(OrderDTO obj)
        {
            Result<OrderDTO> _result = new Result<OrderDTO>();
            try
            {
                Order newRecord = new Order()
                {
                    BuyerID = obj.buyerID,
                    TotalPrice = obj.totalPrice,
                    CreatedDate = System.DateTime.Now,
                    Status = Status.Active.ToInt(),
                    UniqueID = System.Guid.NewGuid()
                };
                _dataContextFactory.Context.Order.Add(newRecord);
                _dataContextFactory.SaveAll();
                _result.TransactionResult = newRecord.ToDTO();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Insert_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
