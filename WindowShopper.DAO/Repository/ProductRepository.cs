﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Product;

namespace WindowShopper.DAO.Repository
{
    public class ProductRepository : Repository<Product>
    {
        public ProductRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<ProductDTO> GetLatestProductsByPage(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.Product
                               select r).OrderByDescending(x => x.ID).ToPagedList(obj._pageNumber, obj._pageSize);
                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> GetLatestProductsByUser_Page(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.Product
                               where r.OwnerID == obj.ownerID
                               select r).OrderByDescending(x => x.ID).ToPagedList(obj._pageNumber, obj._pageSize);
                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> GetProductsByCategory_Page(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                IPagedList<Product> details;
                if (obj.productType > 0)
                {
                    details = (from r in _dataContextFactory.Context.Product
                               where r.ProductType == obj.productType
                               select r).OrderByDescending(x => x.ID).ToPagedList(obj._pageNumber, obj._pageSize);
                }
                else
                {
                    details = (from r in _dataContextFactory.Context.Product
                               select r).OrderByDescending(x => x.ID).ToPagedList(obj._pageNumber, obj._pageSize);
                }

                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> SearchProduct(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.Product
                               where r.Name.Contains(obj.name)
                               select r).OrderByDescending(x => x.ID).ToPagedList(obj._pageNumber, obj._pageSize);
                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> GetProductsInCart(List<int> productIds)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.Product
                               where productIds.Contains(r.ID)
                               select r).OrderByDescending(x => x.ID).ToList();
                if (details != null)
                {
                    _result.TransactionResultList = details.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> GetProductById(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                var detail = (from r in _dataContextFactory.Context.Product
                              where r.ID == obj.ID
                              select r).FirstOrDefault();
                if (detail != null)
                {
                    _result.TransactionResult = detail.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> AddNewProduct(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                Product newRecord = new Product()
                {
                    ProductType = obj.productType,
                    Name = obj.name,
                    Description = obj.description,
                    Quantity = obj.quantity,
                    Price = obj.price,
                    OwnerID = obj.ownerID,
                    CreatedDate = System.DateTime.Now,
                    Status = Status.Active.ToInt(),
                    UniqueID = System.Guid.NewGuid()
                };
                _dataContextFactory.Context.Product.Add(newRecord);
                _dataContextFactory.SaveAll();
                _result.TransactionResult = newRecord.ToDTO();
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Insert_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<ProductDTO> UpdatePcitureList(ProductDTO obj)
        {
            Result<ProductDTO> _result = new Result<ProductDTO>();
            try
            {
                var existRecord = (from r in _dataContextFactory.Context.Product
                                   where r.ID == obj.ID
                                   select r).FirstOrDefault();
                if (existRecord != null)
                {
                    existRecord.Pictures = obj._pictureList.ToJson();
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = existRecord.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Update_Success);
                }
                else
                {
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Update_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Update_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
