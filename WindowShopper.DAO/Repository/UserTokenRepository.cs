﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper;
using WindowShopper.DAL.DatabaseModel;
using WindowShopper.DAO;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.UserToken;

namespace WindowShopper.DAO.Repository
{
    public class UserTokenRepository : Repository<UserToken>
    {
        public UserTokenRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<UserTokenDTO> AddUserToken(UserTokenDTO obj)
        {
            Result<UserTokenDTO> _result = new Result<UserTokenDTO>();
            try
            {
                UserToken existRecord = _dataContextFactory.Context.UserToken.Where(x => x.UserID == obj.userID).FirstOrDefault();
                if (existRecord == null)
                {
                    UserToken newRecord = new UserToken()
                    {
                        Token = obj.token,
                        TokenKey = obj.tokenKey,
                        Email = obj.email,
                        UserID = obj.userID,
                        CreatedDate = System.DateTime.Now,
                        Status = Status.Active.ToInt(),
                        UniqueID = Guid.NewGuid()
                    };
                    _dataContextFactory.Context.UserToken.Add(newRecord);
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = newRecord.ToDTO();
                }
                else
                {
                    existRecord.Token = obj.token;
                    existRecord.Email = obj.email;
                    existRecord.UpdatedDate = System.DateTime.Now;
                    existRecord.UserID = obj.userID;
                    existRecord.TokenKey = obj.tokenKey;
                    existRecord.Status = obj._status.ToInt();
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = existRecord.ToDTO();
                }
                _result.IsSucceeded = true;
                _result.ResultMessage.Add(DBContextActions.Insert_Success);
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Insert_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserTokenDTO> RemoveUserToken(UserTokenDTO obj)
        {
            Result<UserTokenDTO> _result = new Result<UserTokenDTO>();
            try
            {
                UserToken existRecord = _dataContextFactory.Context.UserToken.Where(x => x.Token == obj.token).FirstOrDefault();
                if (existRecord != null)
                {
                    _dataContextFactory.Context.UserToken.Remove(existRecord);
                    _dataContextFactory.SaveAll();
                    _result.TransactionResult = existRecord.ToDTO();
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Delete_Success);
                }
                else
                {
                    _result.TransactionResult = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Delete_Fail);
                }
            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Delete_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
        public Result<UserTokenDTO> GetToken(UserTokenDTO obj)
        {
            Result<UserTokenDTO> _result = new Result<UserTokenDTO>();
            try
            {
                var detail = (from r in _dataContextFactory.Context.UserToken
                              where r.Token == obj.token
                              select r).FirstOrDefault();

                if (detail == null)
                {
                    _result.TransactionResult = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResult = (detail != null) ? detail.ToDTO() : null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
