﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.UserTypes;

namespace WindowShopper.DAO.Repository
{
    public class UserTypeRepository : Repository<DAL.DatabaseModel.UserType>
    {
        public UserTypeRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<UserTypeDTO> GetUserTypes()
        {
            Result<UserTypeDTO> _result = new Result<UserTypeDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.UserType
                              select r).ToList();

                if (details == null)
                {
                    _result.TransactionResultList = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResultList = details.ToDTO() ?? null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
