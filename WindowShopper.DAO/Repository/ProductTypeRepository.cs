﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowShopper.DAO.Resources;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.ProductTypes;

namespace WindowShopper.DAO.Repository
{
    public class ProductTypeRepository : Repository<DAL.DatabaseModel.ProductType>
    {
        public ProductTypeRepository(IDataContextFactory _IDataContextFactory)
            : base(_IDataContextFactory)
        {

        }
        public Result<ProductTypeDTO> GetProductTypes()
        {
            Result<ProductTypeDTO> _result = new Result<ProductTypeDTO>();
            try
            {
                var details = (from r in _dataContextFactory.Context.ProductType
                               select r).ToList();

                if (details == null)
                {
                    _result.TransactionResultList = null;
                    _result.IsSucceeded = false;
                    _result.ResultMessage.Add(DBContextActions.Select_Fail);
                }
                else
                {
                    _result.TransactionResultList = details.ToDTO() ?? null;
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add(DBContextActions.Select_Success);
                }

            }
            catch (Exception ex)
            {
                _result.IsSucceeded = false;
                _result.ResultMessage.Add(DBContextActions.Select_Fail);
                _result.ResultMessage.Add(ex.Message);
                _result.ResultMessage.Add(ex.StackTrace);
            }
            return _result;
        }
    }
}
