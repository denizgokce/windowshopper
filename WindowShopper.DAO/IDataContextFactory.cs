using WindowShopper.DAL.DatabaseModel;

namespace WindowShopper.DAO
{
    public interface IDataContextFactory
    {
        ECommerceEntities Context { get; }
        void SaveAll();
    }
}