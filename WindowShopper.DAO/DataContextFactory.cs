﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using WindowShopper.DAL.DatabaseModel;

namespace WindowShopper.DAO
{
    public class DataContextFactory : IDataContextFactory
    {

        ECommerceEntities _dataContext;

        public DataContextFactory()
        {
            if (_dataContext == null)
            {
                _dataContext = new ECommerceEntities();
            }

        }

        public ECommerceEntities Context
        {
            get
            {
                return _dataContext;
            }
        }

        public void SaveAll()
        {
            //TODO:Need to be implemented
            _dataContext.SaveChanges();
        }
        private bool isDisposed = false;
        private void Dispose(bool isDisposed)
        {
            if (!this.isDisposed)
            {
                if (isDisposed)
                {
                    this.Context.Dispose();
                }
            }

            isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}
