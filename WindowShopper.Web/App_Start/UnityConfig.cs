using System;

using Unity;
using Unity.Injection;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.Services.Base;
using WindowShopper.Services.Image;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();
            container.RegisterType<IDataContextFactory, DataContextFactory>();

            container.RegisterType<MessageRepository, MessageRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<OrderedProductRepository, OrderedProductRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<OrderRepository, OrderRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<ProductRepository, ProductRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<ProductTypeRepository, ProductTypeRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<UserInformationRepository, UserInformationRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<UserRepository, UserRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<UserTokenRepository, UserTokenRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));
            container.RegisterType<UserTypeRepository, UserTypeRepository>(new InjectionConstructor(container.Resolve<IDataContextFactory>()));

            container.RegisterType<SharedLogic, SharedLogic>(new InjectionConstructor(container.Resolve<UserTokenRepository>()));
            container.RegisterType<BaseService, ImageService>();
        }
    }
}