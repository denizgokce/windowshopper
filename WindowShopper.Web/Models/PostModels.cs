﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WindowShopper.Web.Models
{
    public class LoginUser
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class CartProduct
    {
        public int productId { get; set; }
        public int quantity { get; set; }
    }
    public class NewUser
    {
        public string username { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
    public class NewMessage
    {
        public string username { get; set; }
        public string title { get; set; }
        public string message { get; set; }
    }
    public class NewProduct
    {
        public string productImageSmall { get; set; }
        public string productImageDefault { get; set; }
        public string productImageBig { get; set; }
        public int productType { get; set; }
        public string productName { get; set; }
        public string productDescription { get; set; }
        public float productPrice { get; set; }
        public int productQuantity { get; set; }
    }
    public class UpdateSetting
    {
        public string username { get; set; }
        public string password { get; set; }
        public string email { get; set; }
        public int userType { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime birthDate { get; set; }
        public string phoneNumber { get; set; }
        public string ssn { get; set; }
    }
}