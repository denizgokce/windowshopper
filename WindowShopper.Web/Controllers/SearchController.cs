﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class SearchController : BaseController
    {
        private readonly ProductRepository _productRepository;
        public SearchController(ProductRepository productRepository, MessageRepository messageRepository, SharedLogic sharedLogic)
            : base(messageRepository, sharedLogic)
        {
            this._productRepository = productRepository;
        }
        // GET: Search
        public ActionResult Index(string searchText)
        {
            ViewBag.SearchText = searchText;
            Result<DTO.Product.ProductDTO> _resultProduct;
            _resultProduct = _productRepository.SearchProduct(new DTO.Product.ProductDTO()
            {
                _pageNumber = 1,
                _pageSize = 10,
                name = searchText
            });
            if (_resultProduct.IsSucceeded)
            {
                ViewBag.Products = _resultProduct.TransactionResultList;
            }
            return View();
        }
    }
}