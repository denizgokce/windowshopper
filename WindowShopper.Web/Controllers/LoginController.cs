﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Token;
using WindowShopper.DTO.User;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class LoginController : BaseController
    {
        private readonly UserRepository _userRepository;
        private readonly UserTokenRepository _userTokenRepository;
        private readonly SharedLogic _sharedLogic;
        public LoginController(UserRepository userRepository, UserTokenRepository userTokenRepository, SharedLogic sharedLogic, MessageRepository messageRepository)
            : base(messageRepository, sharedLogic)
        {
            this._userRepository = userRepository;
            this._userTokenRepository = userTokenRepository;
            this._sharedLogic = sharedLogic;
        }
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoginUser(LoginUser user)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser == null)
            {
                Result<DTO.User.UserDTO> _resultUser;
                _resultUser = _userRepository.UserLogin(new DTO.User.UserDTO()
                {
                    username = user.username,
                    email = user.username,
                    password = user.password.PasswordEncrypt()
                });
                Result<DTO.UserToken.UserTokenDTO> _resultUserToken = new Result<DTO.UserToken.UserTokenDTO>();
                if (_resultUser.IsSucceeded)
                {
                    Token _token = new Token()
                    {
                        UniqueKey = Guid.NewGuid(),
                        User = _resultUser.TransactionResult,
                        ExpireDate = System.DateTime.Now.AddMonths(1),
                        IsMobile = false
                    };
                    TokenValue _tokenValue = _token.ToToken();
                    _resultUserToken = _userTokenRepository.AddUserToken(new DTO.UserToken.UserTokenDTO()
                    {
                        token = _tokenValue.Token,
                        tokenKey = _tokenValue.UniqueKey,
                        email = _resultUser.TransactionResult.email,
                        userID = _resultUser.TransactionResult.ID
                    });
                    if (_resultUserToken.IsSucceeded)
                    {
                        _sharedLogic.SetTokenToCookie(_resultUserToken.TransactionResult.token);
                    }
                }
                return Json(_resultUserToken);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }

        }
        [HttpPost]
        public ActionResult LogoutUser()
        {
            Result<String> _result = new Result<string>()
            {
                IsSucceeded = false,
                ResultMessage = new List<string>()
            };
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null)
            {
                if (_sharedLogic.DeleteToken())
                {
                    _result.IsSucceeded = true;
                    _result.ResultMessage.Add("User Logout Succesfully");
                    _result.TransactionResult = "Success";
                }
            }
            return Json(_result);
        }
        [HttpPost]
        public ActionResult SignUp(NewUser newUser)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser == null)
            {
                Result<DTO.User.UserDTO> _resultUser;
                _resultUser = _userRepository.AddUser(new DTO.User.UserDTO()
                {
                    username = newUser.username,
                    password = newUser.password,
                    email = newUser.email,
                    _userType = UserType.Buyer,
                });
                Result<DTO.UserToken.UserTokenDTO> _resultUserToken = new Result<DTO.UserToken.UserTokenDTO>();
                if (_resultUser.IsSucceeded)
                {
                    Token _token = new Token()
                    {
                        UniqueKey = Guid.NewGuid(),
                        User = _resultUser.TransactionResult,
                        ExpireDate = System.DateTime.Now.AddMonths(1),
                        IsMobile = false
                    };
                    TokenValue _tokenValue = _token.ToToken();
                    _resultUserToken = _userTokenRepository.AddUserToken(new DTO.UserToken.UserTokenDTO()
                    {
                        token = _tokenValue.Token,
                        tokenKey = _tokenValue.UniqueKey,
                        email = _resultUser.TransactionResult.email,
                        userID = _resultUser.TransactionResult.ID
                    });
                    if (_resultUserToken.IsSucceeded)
                    {
                        _sharedLogic.SetTokenToCookie(_resultUserToken.TransactionResult.token);
                    }
                }
                return Json(_resultUserToken);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
    }
}