﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class ProfileController : BaseController
    {
        private readonly UserRepository _userRepository;
        private readonly ProductRepository _productRepository;
        public ProfileController(UserRepository repository, ProductRepository productRepository, MessageRepository messageRepository, SharedLogic sharedLogic)
            : base(messageRepository, sharedLogic)
        {
            this._userRepository = repository;
            this._productRepository = productRepository;
        }
        // GET: Profile
        public ActionResult Index(string username)
        {
            if (username == null || username == "Index")
            {
                return RedirectToAction("Index", "Home");
            }
            Result<DTO.User.UserDTO> _resultUser;
            _resultUser = _userRepository.GetUserByUsername(new DTO.User.UserDTO()
            {
                username = username
            });
            if (_resultUser.IsSucceeded)
            {
                ViewBag.User = _resultUser.TransactionResult;
                Result<DTO.Product.ProductDTO> _resultProduct;
                _resultProduct = _productRepository.GetLatestProductsByUser_Page(new DTO.Product.ProductDTO()
                {
                    ownerID = _resultUser.TransactionResult.ID,
                    _pageNumber = 1,
                    _pageSize = 10
                });
                if (_resultProduct.IsSucceeded)
                {
                    ViewBag.Products = _resultProduct.TransactionResultList;
                }
            }
            return View();
        }
    }
}