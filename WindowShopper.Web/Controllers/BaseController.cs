﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Message;
using WindowShopper.DTO.User;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public abstract class BaseController : Controller
    {
        private readonly MessageRepository _messageRepository;
        private readonly SharedLogic _sharedLogic;
        public BaseController(MessageRepository messageRepository, SharedLogic sharedLogic)
        {
            this._messageRepository = messageRepository;
            this._sharedLogic = sharedLogic;
            Initialize();
        }
        public void Initialize()
        {
            UserDTO _activeUser = _sharedLogic.GetActiveUser();
            Result<int> _resultMessage = new Result<int>();
            _resultMessage.TransactionResult = 0;
            if (_activeUser != null)
            {
                _resultMessage = _messageRepository.GetUnreadMessageCount(new MessageDTO()
                {
                    toID = _activeUser.ID
                });
            }
            ViewBag.activeUser = _activeUser;
            ViewBag.resultMessage = _resultMessage;
            ViewBag.sharedLogic = _sharedLogic;
        }
    }
}