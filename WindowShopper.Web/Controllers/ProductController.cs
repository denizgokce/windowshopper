﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.User;
using WindowShopper.Services.Image;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class ProductController : BaseController
    {
        private readonly ProductRepository _productRepository;
        private readonly ProductTypeRepository _productTypeRepository;
        private readonly SharedLogic _sharedLogic;
        private readonly ImageService _imageService;
        public ProductController(ProductRepository productRepository, ProductTypeRepository productTypeRepository, MessageRepository messageRepository, SharedLogic sharedLogic, ImageService imageService)
            : base(messageRepository, sharedLogic)
        {
            this._productRepository = productRepository;
            this._productTypeRepository = productTypeRepository;
            this._sharedLogic = sharedLogic;
            this._imageService = imageService;
        }
        // GET: Product
        public ActionResult Index(int? id)
        {
            int catId = id ?? 0;
            Result<DTO.ProductTypes.ProductTypeDTO> _resultProductTypes;
            _resultProductTypes = _productTypeRepository.GetProductTypes();
            if (_resultProductTypes.IsSucceeded)
            {
                ViewBag.ProductTypes = _resultProductTypes.TransactionResultList;
            }
            Result<DTO.Product.ProductDTO> _resultProduct;
            _resultProduct = _productRepository.GetProductsByCategory_Page(new DTO.Product.ProductDTO()
            {
                _pageNumber = 1,
                _pageSize = 10,
                productType = catId
            });
            if (_resultProduct.IsSucceeded)
            {
                ViewBag.Products = _resultProduct.TransactionResultList;
            }
            return View();
        }
        [HttpGet]
        public ActionResult ProductDetail(int? id)
        {
            int pId = id ?? 0;
            if (pId == 0)
            {
                return RedirectToAction("Index", "Home");
            }
            Result<DTO.ProductTypes.ProductTypeDTO> _resultProductTypes;
            _resultProductTypes = _productTypeRepository.GetProductTypes();
            if (_resultProductTypes.IsSucceeded)
            {
                ViewBag.ProductTypes = _resultProductTypes.TransactionResultList;
            }
            Result<DTO.Product.ProductDTO> _resultProduct;
            _resultProduct = _productRepository.GetProductById(new DTO.Product.ProductDTO()
            {
                ID = pId
            });
            if (_resultProduct.IsSucceeded)
            {
                ViewBag.Product = _resultProduct.TransactionResult;
            }
            return View();
        }
        [HttpGet]
        public ActionResult AddProduct()
        {
            Result<DTO.ProductTypes.ProductTypeDTO> _resultProductTypes;
            _resultProductTypes = _productTypeRepository.GetProductTypes();
            if (_resultProductTypes.IsSucceeded)
            {
                ViewBag.ProductTypes = _resultProductTypes.TransactionResultList;
            }
            return View();
        }
        [HttpPost]
        public ActionResult AddNewProduct(NewProduct newProduct)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null && activeUser._userType == UserType.Seller)
            {
                Result<DTO.Product.ProductDTO> _resultProduct;
                _resultProduct = _productRepository.AddNewProduct(new DTO.Product.ProductDTO()
                {
                    productType = newProduct.productType,
                    name = newProduct.productName,
                    description = newProduct.productDescription,
                    quantity = newProduct.productQuantity,
                    price = newProduct.productPrice,
                    ownerID = activeUser.ID
                });
                if (_resultProduct.IsSucceeded)
                {
                    Result<String> _resultImageService = new Result<string>();
                    _resultImageService = _imageService.SaveProductPictures(newProduct.productImageDefault, newProduct.productImageBig, newProduct.productImageSmall, _resultProduct.TransactionResult);
                    if (_resultImageService.IsSucceeded)
                    {
                        _resultProduct = _productRepository.UpdatePcitureList(new DTO.Product.ProductDTO()
                        {
                            ID = _resultProduct.TransactionResult.ID,
                            _pictureList = _resultImageService.TransactionResultList
                        });
                        if (_resultProduct.IsSucceeded)
                        {
                            return RedirectToAction("Index", "Home");
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
    }
}