﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class CartController : BaseController
    {
        private readonly SharedLogic _sharedLogic;
        private readonly ProductRepository _productRepository;
        public CartController(ProductRepository productRepository, SharedLogic sharedLogic, MessageRepository messageRepository)
            : base(messageRepository, sharedLogic)
        {
            this._productRepository = productRepository;
            this._sharedLogic = sharedLogic;
        }
        // GET: Cart
        public ActionResult Index()
        {
            Result<DTO.Product.ProductDTO> _resultProduct;
            _resultProduct = _productRepository.GetProductsInCart(_sharedLogic.GetCart(HttpContext).Select(x => x.productId).ToList());
            if (_resultProduct.IsSucceeded)
            {
                ViewBag.Products = _resultProduct.TransactionResultList;
            }
            ViewBag.Cart = _sharedLogic.GetCart(HttpContext);
            return View();
        }
        [HttpPost]
        public ActionResult AddToCart(CartProduct product)
        {
            _sharedLogic.AddToCart(product, HttpContext);
            return Json(true);
        }
        [HttpPost]
        public ActionResult RemoveFromCart(CartProduct product)
        {
            _sharedLogic.RemoveFromCart(product.productId, HttpContext);
            return Json(true);
        }
        [HttpPost]
        public ActionResult UpdateProductInCart(CartProduct product)
        {
            _sharedLogic.UpdateCart(product, HttpContext);
            return Json(true);
        }
    }
}