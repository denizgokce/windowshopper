﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.User;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class MessageController : BaseController
    {
        private readonly UserRepository _userRepository;
        private readonly MessageRepository _messageRepository;
        private readonly SharedLogic _sharedLogic;
        public MessageController(UserRepository userRepository, MessageRepository messageRepository, SharedLogic sharedLogic)
            : base(messageRepository, sharedLogic)
        {
            this._userRepository = userRepository;
            this._messageRepository = messageRepository;
            this._sharedLogic = sharedLogic;
        }
        // GET: Checkout
        // GET: Message
        public ActionResult Index(string messageType)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null)
            {
                if (String.IsNullOrEmpty(messageType) || messageType == "recieved")
                {
                    Result<DTO.Message.MessageDTO> _resultMessage;
                    _resultMessage = _messageRepository.GetLatestRecievedMessagesByPage(new DTO.Message.MessageDTO()
                    {
                        toID = activeUser.ID,
                        _pageNumber = 1,
                        _pageSize = 10
                    });
                    if (_resultMessage.IsSucceeded)
                    {
                        ViewBag.Messages = _resultMessage.TransactionResultList;
                        ViewBag.MessageType = "Recieved";
                    }

                }
                else
                {
                    Result<DTO.Message.MessageDTO> _resultMessage;
                    _resultMessage = _messageRepository.GetLatestSentMessagesByPage(new DTO.Message.MessageDTO()
                    {
                        fromID = activeUser.ID,
                        _pageNumber = 1,
                        _pageSize = 10
                    });
                    if (_resultMessage.IsSucceeded)
                    {
                        ViewBag.Messages = _resultMessage.TransactionResultList;
                        ViewBag.MessageType = "Sent";
                    }

                }
                Result<DTO.Message.MessageDTO> _resultMessage2;
                _resultMessage2 = _messageRepository.UpdateUnreadMessages(new DTO.Message.MessageDTO()
                {
                    toID = activeUser.ID
                });
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpGet]
        public ActionResult NewMessage(string username)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null && activeUser.username != username)
            {
                ViewBag.ToUsername = username;
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult SendNewMessage(NewMessage newMessage)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null)
            {
                Result<DTO.User.UserDTO> _resultUser;
                _resultUser = _userRepository.GetUserByUsername(new DTO.User.UserDTO()
                {
                    username = newMessage.username
                });
                if (_resultUser.IsSucceeded)
                {
                    Result<DTO.Message.MessageDTO> _resultMessage;
                    _resultMessage = _messageRepository.AddMessage(new DTO.Message.MessageDTO()
                    {
                        fromID = activeUser.ID,
                        toID = _resultUser.TransactionResult.ID,
                        title = newMessage.title,
                        content = newMessage.message
                    });
                    if (_resultMessage.IsSucceeded)
                    {
                        return Json(_resultMessage);
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return Json(null);
        }
    }
}