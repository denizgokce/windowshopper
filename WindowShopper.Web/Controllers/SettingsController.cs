﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.User;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class SettingsController : BaseController
    {
        private readonly UserRepository _userRepository;
        private readonly UserInformationRepository _userInformationRepository;
        private readonly UserTypeRepository _userTypeRepository;
        private readonly SharedLogic _sharedLogic;
        public SettingsController(UserRepository userRepository, UserInformationRepository userInformationRepository, UserTypeRepository userTypeRepository, SharedLogic sharedLogic, MessageRepository messageRepository)
            : base(messageRepository, sharedLogic)
        {
            this._userRepository = userRepository;
            this._userInformationRepository = userInformationRepository;
            this._userTypeRepository = userTypeRepository;
            this._sharedLogic = sharedLogic;
        }
        // GET: Settings
        public ActionResult Index()
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null)
            {
                Result<DTO.UserTypes.UserTypeDTO> _resultUserTypes;
                _resultUserTypes = _userTypeRepository.GetUserTypes();
                if (_resultUserTypes.IsSucceeded)
                {
                    ViewBag.UserTypes = _resultUserTypes.TransactionResultList;
                }
                Result<DTO.User.UserDTO> _resultUser;
                _resultUser = _userRepository.GetUser(new UserDTO()
                {
                    ID = activeUser.ID
                });
                if (_resultUser.IsSucceeded)
                {
                    ViewBag.User = _resultUser.TransactionResult;
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return View();
        }
        [HttpPost]
        public ActionResult UpdateSettings(UpdateSetting setting)
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            if (activeUser != null)
            {
                Result<DTO.User.UserDTO> _resultUser;
                _resultUser = _userRepository.UpdateUser(new UserDTO()
                {
                    ID = activeUser.ID,
                    username = setting.username,
                    email = setting.email,
                    userType = setting.userType,
                    password = setting.password.PasswordEncrypt()
                });
                if (_resultUser.IsSucceeded)
                {
                    Result<WindowShopper.DTO.UserInformation.UserInformationDTO> _resultUserInformation;
                    _resultUserInformation = _userInformationRepository.UpdateUserInformation(new WindowShopper.DTO.UserInformation.UserInformationDTO()
                    {
                        ID = _resultUser.TransactionResult.ID,
                        firstName = setting.firstName,
                        lastName = setting.lastName,
                        profilePicture = "",
                        SSN = setting.ssn,
                        dateOfBirth = setting.birthDate,
                        phoneNumber = setting.phoneNumber
                    });
                    if (_resultUser.IsSucceeded)
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
            return Json(null);
        }
    }
}