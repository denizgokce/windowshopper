﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.User;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class CheckoutController : BaseController
    {
        private readonly ProductRepository _productRepository;
        private readonly OrderedProductRepository _orderedProductRepository;
        private readonly OrderRepository _orderRepository;
        private readonly SharedLogic _sharedLogic;

        public CheckoutController(ProductRepository productRepository, OrderedProductRepository orderedProductRepository, OrderRepository orderRepository, SharedLogic sharedLogic, MessageRepository messageRepository)
            : base(messageRepository, sharedLogic)
        {
            this._productRepository = productRepository;
            this._orderedProductRepository = orderedProductRepository;
            this._orderRepository = orderRepository;
            this._sharedLogic = sharedLogic;
        }
        // GET: Checkout
        public ActionResult Index()
        {
            Result<DTO.Product.ProductDTO> _resultProduct;
            _resultProduct = _productRepository.GetProductsInCart(_sharedLogic.GetCart(HttpContext).Select(x => x.productId).ToList());
            if (_resultProduct.IsSucceeded)
            {
                ViewBag.Products = _resultProduct.TransactionResultList;
            }
            ViewBag.Cart = _sharedLogic.GetCart(HttpContext);
            return View();
        }
        [HttpPost]
        public ActionResult MakePurchase()
        {
            UserDTO activeUser = _sharedLogic.GetActiveUser();
            List<CartProduct> cart = _sharedLogic.GetCart(HttpContext);

            if (activeUser != null)
            {
                Result<DTO.Product.ProductDTO> _resultProduct;
                _resultProduct = _productRepository.GetProductsInCart(_sharedLogic.GetCart(HttpContext).Select(x => x.productId).ToList());
                if (_resultProduct.IsSucceeded)
                {
                    double totalPrice = 0;
                    cart.ToList().ForEach(x =>
                    {
                        totalPrice += (_resultProduct.TransactionResultList.Find(y => y.ID == x.productId).price * x.quantity);
                    });
                    Result<DTO.Order.OrderDTO> _resultOrder;
                    _resultOrder = _orderRepository.AddOrder(new DTO.Order.OrderDTO()
                    {
                        buyerID = activeUser.ID,
                        totalPrice = totalPrice
                    });
                    if (_resultOrder.IsSucceeded)
                    {
                        Result<DTO.OrderProduct.OrderedProductDTO> _resultOrderedProduct;
                        List<DTO.OrderProduct.OrderedProductDTO> orderedProducts = new List<DTO.OrderProduct.OrderedProductDTO>();
                        foreach (var item in cart)
                        {
                            orderedProducts.Add(new DTO.OrderProduct.OrderedProductDTO()
                            {
                                orderID = _resultOrder.TransactionResult.ID,
                                productID = item.productId,
                                quantity = item.quantity,
                                productTotalPrice = item.quantity * _resultProduct.TransactionResultList.Find(y => y.ID == item.productId).price
                            });
                        }
                        _resultOrderedProduct = _orderedProductRepository.AddOrderedProducts(orderedProducts);
                        if (_resultOrderedProduct.IsSucceeded)
                        {
                            return Json(_resultOrderedProduct);
                        }
                    }
                }
            }
            else
            {
                return RedirectToAction("Index", "Checkout");
            }
            return Json(null);
        }
    }
}