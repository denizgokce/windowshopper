﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO.Repository;
using WindowShopper.Web.Utilities;

namespace WindowShopper.Web.Controllers
{
    public class ContactController : BaseController
    {
        public ContactController(MessageRepository messageRepository, SharedLogic sharedLogic)
            : base(messageRepository, sharedLogic)
        {

        }
        // GET: Contact
        public ActionResult Index()
        {
            return View();
        }
    }
}