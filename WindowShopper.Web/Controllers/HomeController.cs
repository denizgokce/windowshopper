﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.Web.Utilities;
using WindowShopper.Web.Utilities.Filters;

namespace WindowShopper.Web.Controllers
{
    public class HomeController : BaseController
    {
        private readonly ProductRepository _productRepository;
        private readonly ProductTypeRepository _productTypeRepository;
        public HomeController(ProductRepository productRepository, ProductTypeRepository productTypeRepository, MessageRepository messageRepository, SharedLogic sharedLogic)
            : base(messageRepository, sharedLogic)
        {
            this._productRepository = productRepository;
            this._productTypeRepository = productTypeRepository;
        }
        public ActionResult Index()
        {
            Result<DTO.ProductTypes.ProductTypeDTO> _resultProductTypes;
            _resultProductTypes = _productTypeRepository.GetProductTypes();
            if (_resultProductTypes.IsSucceeded)
            {
                ViewBag.ProductTypes = _resultProductTypes.TransactionResultList;
            }
            Result<DTO.Product.ProductDTO> _resultProduct;
            _resultProduct = _productRepository.GetLatestProductsByPage(new DTO.Product.ProductDTO()
            {
                _pageNumber = 1,
                _pageSize = 10
            });
            if (_resultProduct.IsSucceeded)
            {
                ViewBag.Products = _resultProduct.TransactionResultList;
            }
            return View();
        }
    }
}