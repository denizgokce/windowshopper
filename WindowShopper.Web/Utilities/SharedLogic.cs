﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Product;
using WindowShopper.DTO.Token;
using WindowShopper.DTO.User;
using WindowShopper.Web.Models;
using WindowShopper.Web.Utilities.Helpers;
using static WindowShopper.Web.Utilities.Helpers.SessionHelper;

namespace WindowShopper.Web.Utilities
{
    public class SharedLogic
    {
        private readonly UserTokenRepository _userTokenRepository;
        public SharedLogic(UserTokenRepository userTokenRepository)
        {
            this._userTokenRepository = userTokenRepository;
        }
        private const string cookiekey = "windowshopper_activeuser";
        private readonly CookieHelper _cookieHelper = new CookieHelper();
        private const string cartsessionkey = "windowshopper_cartsession";
        public void SetTokenToCookie(string token)
        {
            _cookieHelper.SetCookie(cookiekey, token, 30);
        }
        public string GetTokenFromCookie()
        {
            string token = _cookieHelper.GetCookie(cookiekey);
            if (!String.IsNullOrEmpty(token))
            {
                return token;
            }
            else
            {
                return null;
            }
        }
        public bool DeleteToken()
        {
            string token = _cookieHelper.GetCookie(cookiekey);
            if (!String.IsNullOrEmpty(token))
            {
                Result<DTO.UserToken.UserTokenDTO> _resultUserToken;
                _resultUserToken = _userTokenRepository.RemoveUserToken(new DTO.UserToken.UserTokenDTO()
                {
                    token = token
                });
                _cookieHelper.RemoveCookie(cookiekey);
                return true;
            }
            return false;
        }
        public UserDTO GetActiveUser()
        {
            string token = GetTokenFromCookie();
            if (!String.IsNullOrEmpty(token))
            {
                Result<DTO.UserToken.UserTokenDTO> _resultUserToken;
                _resultUserToken = _userTokenRepository.GetToken(new DTO.UserToken.UserTokenDTO()
                {
                    token = token
                });
                if (!_resultUserToken.IsSucceeded)
                {
                    return null;
                }
                TokenValue _tokenValue = new TokenValue()
                {
                    Token = _resultUserToken.TransactionResult.token,
                    UniqueKey = _resultUserToken.TransactionResult.tokenKey
                };
                Token _token = _tokenValue.FromToken();
                return _token.User;
            }
            else
            {
                return null;
            }
        }
        public void AddToCart(CartProduct cartProduct, HttpContextBase _context)
        {
            ISessionHelper _sessionHelper = new HttpContextBaseSessionHelper(_context);
            string cartInSession = _sessionHelper.Get<string>(cartsessionkey);
            if (String.IsNullOrEmpty(cartInSession))
            {
                List<CartProduct> newProductListInCart = new List<CartProduct>() { cartProduct };
                _sessionHelper.Set<string>(cartsessionkey, newProductListInCart.ToJson());
            }
            else
            {
                List<CartProduct> existProductListInCart = cartInSession.ToType<List<CartProduct>>();
                CartProduct existincart = existProductListInCart.Find(x => x.productId == cartProduct.productId);
                if (existincart == null)
                    existProductListInCart.Add(cartProduct);
                else
                    existincart.quantity++;
                _sessionHelper.Set<string>(cartsessionkey, existProductListInCart.ToJson());
            }
        }
        public void RemoveFromCart(int productId, HttpContextBase _context)
        {
            ISessionHelper _sessionHelper = new HttpContextBaseSessionHelper(_context);
            string cartInSession = _sessionHelper.Get<string>(cartsessionkey);
            if (!String.IsNullOrEmpty(cartInSession))
            {
                List<CartProduct> existProductListInCart = cartInSession.ToType<List<CartProduct>>();
                existProductListInCart.Remove(existProductListInCart.Find(x => x.productId == productId));
                _sessionHelper.Set<string>(cartsessionkey, existProductListInCart.ToJson());
            }
        }
        public void UpdateCart(CartProduct cartProduct, HttpContextBase _context)
        {
            ISessionHelper _sessionHelper = new HttpContextBaseSessionHelper(_context);
            string cartInSession = _sessionHelper.Get<string>(cartsessionkey);
            if (!String.IsNullOrEmpty(cartInSession))
            {
                List<CartProduct> existProductListInCart = cartInSession.ToType<List<CartProduct>>();
                var product = existProductListInCart.Find(x => x.productId == cartProduct.productId);
                product.quantity = cartProduct.quantity;
                _sessionHelper.Set<string>(cartsessionkey, existProductListInCart.ToJson());
            }
        }
        public List<CartProduct> GetCart(HttpContextBase _context)
        {
            ISessionHelper _sessionHelper = new HttpContextBaseSessionHelper(_context);
            string cartInSession = _sessionHelper.Get<string>(cartsessionkey);
            if (!String.IsNullOrEmpty(cartInSession))
            {
                return cartInSession.ToType<List<CartProduct>>();
            }
            return new List<CartProduct>();
        }
        public void DeleteCart(HttpContextBase _context)
        {
            ISessionHelper _sessionHelper = new HttpContextBaseSessionHelper(_context);
            string cartInSession = _sessionHelper.Get<string>(cartsessionkey);
            if (!String.IsNullOrEmpty(cartInSession))
            {
                _sessionHelper.RemoveAt(cartsessionkey);
            }
        }
        public string GetTimeDiffInString(DateTime CreatedDate)
        {
            TimeSpan span = (DateTime.Now - CreatedDate);
            if (span.Days > 0)
            {
                return span.Days.ToString() + " Days Ago";
            }
            if (span.Hours > 0)
            {
                return span.Hours.ToString() + " Hours Ago";
            }
            if (span.Minutes > 0)
            {
                return span.Minutes.ToString() + " Minutes Ago";
            }
            if (span.Seconds > 0)
            {
                return span.Seconds.ToString() + " Seconds Ago";
            }
            return CreatedDate.ToString();
        }
    }
}