﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using WindowShopper.DTO.Token;
using WindowShopper.Utilities.Security;

namespace WindowShopper.Web.Utilities
{
    public static class Extension
    {
        public static int ToInt(this object obj)
        {
            return Convert.ToInt32(obj);
        }
        public static double ToDouble(this object obj)
        {
            return Convert.ToDouble(obj);
        }
        public static string ToJson(this Object obj)
        {
            return JsonConvert.SerializeObject(obj);
        }
        public static T ToType<T>(this string str)
        {
            return JsonConvert.DeserializeObject<T>(str);
        }
        public static TokenValue ToToken(this Token token)
        {
            TokenValue obj = new TokenValue();
            Guid Key;
            obj.Token = Cryptography.Encrypt(token.ToJson(), out Key);
            obj.UniqueKey = Key;
            return obj;
        }
        public static Token FromToken(this TokenValue token)
        {
            return Cryptography.Decrypt(token.Token, token.UniqueKey.ToString()).ToType<Token>();
        }
        public static string ToUrlEncoded(this string url)
        {
            return HttpUtility.UrlEncode(url);
        }
        public static string ToUrlDecoded(this string url)
        {
            return HttpUtility.UrlDecode(url);
        }
        public static string ToHTMLEncoded(this string url)
        {
            return HttpUtility.HtmlEncode(url);
        }
        public static string ToHTMLDecoded(this string url)
        {
            return HttpUtility.HtmlDecode(url);
        }
        public static T ToEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }
        public static bool IsValidMail(this string email)
        {
            try
            {
                MailAddress m = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }
}