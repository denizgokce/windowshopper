﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WindowShopper.Web.Utilities.Helpers
{
    public class SessionHelper
    {
        public interface ISessionHelper
        {
            T Get<T>(string key);
            void Set<T>(string key,T value);
            void RemoveAt(string key);
        }

        public class HttpContextSessionHelper : ISessionHelper
        {
            private readonly HttpContext _context;
            
            public HttpContextSessionHelper(HttpContext context)
            {
                _context = context;
            }

            public T Get<T>(string key)
            {
                object value =null;
                if (_context.Session != null)
                    value = _context.Session[key];
                return value == null ? default(T) : (T)value;
            }

            public void Set<T>(string key, T value)
            {
                _context.Session[key] = value;
            }
            public void RemoveAt(string key)
            {
                _context.Session.Remove(key);
            }
        }
        public class HttpContextBaseSessionHelper : ISessionHelper
        {
            private readonly HttpContextBase _context;

            public HttpContextBaseSessionHelper(HttpContextBase context)
            {
                _context = context;
            }

            public T Get<T>(string key)
            {
                object value = _context.Session[key];
                return value == null ? default(T) : (T)value;
            }

            public void Set<T>(string key, T value)
            {
                _context.Session[key] = value;
            }
            public void RemoveAt(string key)
            {
                _context.Session.Remove(key);
            }
        }
    }
}