﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using WindowShopper.DAO;
using WindowShopper.DAO.Repository;
using WindowShopper.DTO.Generic;
using WindowShopper.DTO.Token;

namespace WindowShopper.Web.Utilities.Filters
{
    public class AuthenticationFilter : ActionFilterAttribute
    {
        //public static AbstractModel Authenticate(AbstractModel model)
        //{
        //    return null;
        //}
        private readonly UserRepository _userRepository;
        private readonly UserTokenRepository _userTokenRepository;
        private readonly SharedLogic _sharedLogic;
        public AuthenticationFilter(UserRepository userRepository, UserTokenRepository userTokenRepository, SharedLogic sharedLogic)
        {
            this._userRepository = userRepository;
            this._userTokenRepository = userTokenRepository;
            this._sharedLogic = sharedLogic;
        }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //var controllerName = filterContext.RouteData.Values["controller"];
            //var actionName = filterContext.RouteData.Values["action"];
            //var message = String.Format("{0} controller:{1} action:{2}", "onactionexecuting", controllerName, actionName);
            //Debug.WriteLine(message, "Action Filter Log");
            //var token = filterContext.HttpContext.Request.Params["token"];
            Boolean result = false;
            string token = _sharedLogic.GetTokenFromCookie();
            if (!String.IsNullOrEmpty(token))
            {
                Token _token = null;
                Result<DTO.UserToken.UserTokenDTO> _resultUserToken;
                _resultUserToken = _userTokenRepository.GetToken(new DTO.UserToken.UserTokenDTO()
                {
                    token = token
                });
                if (!_resultUserToken.IsSucceeded)
                {
                    result = false;
                    _sharedLogic.DeleteToken();
                }
                else
                {
                    TokenValue _tokenValue = new TokenValue()
                    {
                        Token = _resultUserToken.TransactionResult.token,
                        UniqueKey = _resultUserToken.TransactionResult.tokenKey
                    };
                    _token = _tokenValue.FromToken();
                    Result<DTO.User.UserDTO> _resultUser;
                    _resultUser = _userRepository.GetUser(new DTO.User.UserDTO()
                    {
                        ID = _token.User.ID
                    });
                    if (!_resultUser.IsSucceeded)
                    {
                        result = false;
                    }
                    if (_resultUser.TransactionResult._status != Status.Active)
                    {
                        result = false;
                    }
                    if (_token.ExpireDate < DateTime.Now)
                    {
                        result = false;
                    }
                    result = true;
                }
                if (!result)
                {
                    filterContext.Result = new RedirectToRouteResult(
                            new RouteValueDictionary
                            {
                                  { "controller", "Login" },
                                  { "action", "Index" }
                            });
                }
                else
                {
                    filterContext.ActionParameters["token"] = _token;
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(
                           new RouteValueDictionary
                           {
                                  { "controller", "Login" },
                                  { "action", "Index" }
                           });
            }
            base.OnActionExecuting(filterContext);
        }
        //public override void OnResultExecuting(ResultExecutingContext filterContext)
        //{
        //    //You may fetch data from database here 
        //    filterContext.Controller.ViewBag.GreetMesssage = "Hello Foo";
        //    base.OnResultExecuting(filterContext);
        //}

    }
}